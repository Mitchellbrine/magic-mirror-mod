package mc.Mitchellbrine.magicMirror;

import mc.Mitchellbrine.magicMirror.util.SnazzyItem;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.common.Configuration;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;


@Mod(modid = "magicMirror", name = "Magic Mirror Mod", version = "1.0")
public class MainMod {

	public static int magicMirrorID;
	public static int vialID;
	public static int diamondVialID;
	public static int duplicateVialID;
		
	public static Item magicMirror;
	public static Item vial;
	public static Item diamondVial;
	public static Item duplicateVial;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		Configuration config = new Configuration(event.getSuggestedConfigurationFile());

		config.load();
		
		magicMirrorID = config.get(Configuration.CATEGORY_ITEM, "magicMirrorID", 6000).getInt();
		vialID = config.get(Configuration.CATEGORY_ITEM, "vialID", 6001).getInt();
		diamondVialID = config.get(Configuration.CATEGORY_ITEM, "diamondVialID", 6002).getInt();
		duplicateVialID = config.get(Configuration.CATEGORY_ITEM, "duplicateVialID", 6003).getInt();
		
		config.save();
		
	}
	
	
	
	@EventHandler
	public void init(FMLInitializationEvent event) {
		magicMirror = new MagicMirror(magicMirrorID).setCreativeTab(CreativeTabs.tabMisc).setTextureName("magicmirror:magicMirror").setUnlocalizedName("magicmirror:magicMirror");
		vial = new Item(vialID).setCreativeTab(CreativeTabs.tabMaterials).setTextureName("magicmirror:vial").setUnlocalizedName("magicmirror:vial");
		diamondVial = new Item(diamondVialID).setCreativeTab(CreativeTabs.tabMaterials).setTextureName("magicmirror:diamondVial").setUnlocalizedName("magicmirror:diamondVial");
		duplicateVial = new SnazzyItem(duplicateVialID).setCreativeTab(CreativeTabs.tabBrewing).setTextureName("magicmirror:duplicateVial").setUnlocalizedName("magicmirror:duplicateVial");
		
		GameRegistry.registerItem(magicMirror, "magicMirror");
		GameRegistry.registerItem(vial, "vial");
		GameRegistry.registerItem(diamondVial, "diamondVial");
		GameRegistry.registerItem(duplicateVial, "duplicateVial");
		
		LanguageRegistry.addName(magicMirror, "Magic Mirror");
		LanguageRegistry.addName(vial, "Empty Vial");
		LanguageRegistry.addName(diamondVial, "Diamond-filled Vial");
		LanguageRegistry.addName(duplicateVial, EnumChatFormatting.BOLD + "Duplication Solution");
		
		GameRegistry.addRecipe(new ItemStack(magicMirror, 1), " X ", "XYX", " X ", Character.valueOf('X'), Block.obsidian, Character.valueOf('Y'), Item.diamond);
		GameRegistry.addRecipe(new ItemStack(vial, 5), "X X", "X X", " X ", Character.valueOf('X'), Block.thinGlass);
		GameRegistry.addRecipe(new ItemStack(diamondVial, 10), "X", "Y", Character.valueOf('X'), Item.diamond, Character.valueOf('Y'), vial);
		GameRegistry.addRecipe(new ItemStack(magicMirror, 1), "XY", Character.valueOf('X'), new ItemStack(MainMod.magicMirror, 1, 7), Character.valueOf('Y'), diamondVial);
		GameRegistry.addRecipe(new ItemStack(duplicateVial, 1), "X", "Y", Character.valueOf('X'), new ItemStack(Item.appleGold, 1, 0), Character.valueOf('Y'), diamondVial);
		GameRegistry.addRecipe(new ItemStack(magicMirror, 2), "XY", Character.valueOf('Y'), magicMirror, Character.valueOf('X'), duplicateVial);
	}
	
}
