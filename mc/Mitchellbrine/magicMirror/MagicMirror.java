package mc.Mitchellbrine.magicMirror;

import java.util.List;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class MagicMirror extends Item {
	
	public static int mirrorCounter;
	
    public MagicMirror(int par1) {
		super(par1);
		this.setMaxStackSize(1);
		this.setMaxDamage(8);
    
    }

    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
    	if (!par2World.isRemote) {
    		
    		if (par3EntityPlayer instanceof EntityPlayerMP) {
    		EntityPlayerMP EntityPlayer = (EntityPlayerMP) par3EntityPlayer;
    		
    		
    		if (par1ItemStack.getItemDamage() < 1) {
    		if (mirrorCounter == 7) {
    		if (EntityPlayer.getBedLocation(0) != null)
    			{
    			EntityPlayer.playerNetServerHandler.setPlayerLocation(EntityPlayer.getBedLocation(0).posX, EntityPlayer.getBedLocation(0).posY, EntityPlayer.getBedLocation(0).posZ, EntityPlayer.cameraYaw, EntityPlayer.cameraPitch);
    			resetMirror(par3EntityPlayer);
    			}
    		else {
    			EntityPlayer.playerNetServerHandler.setPlayerLocation(par2World.getSpawnPoint().posX, par2World.getSpawnPoint().posY + 10, par2World.getSpawnPoint().posZ, EntityPlayer.cameraYaw, EntityPlayer.cameraPitch);    			
    			resetMirror(par3EntityPlayer);
    		}
    		}
    		if (mirrorCounter != 7)
    		{
    		mirrorCounter++;
    		}
    		}
    		}
    	}
    	else {
    		if (par1ItemStack.getItemDamage() < 1) {
    		par2World.playSound(par3EntityPlayer.posX, par3EntityPlayer.posY, par3EntityPlayer.posZ, "random.fizz", 1.0F, 1.0F, false);
    		}
    	}
    	
    	return par1ItemStack;
    }
    
    public static void resetMirror(EntityPlayer par1EntityPlayer) {
    	par1EntityPlayer.addPotionEffect(new PotionEffect(Potion.resistance.id, 20, 5));
    	ItemStack itemstack = par1EntityPlayer.getCurrentEquippedItem();
        itemstack.damageItem(7, par1EntityPlayer);   		
    	mirrorCounter = 0;
    }
    
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
    {
    	par3List.add(EnumChatFormatting.WHITE + "" + EnumChatFormatting.ITALIC + "Gaze in the mirror to return home.");
    }
    }
